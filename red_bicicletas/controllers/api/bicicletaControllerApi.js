const bicicleta = require('../../models/bicicleta')


exports.bicicleta_list= function(req,res){
    bicicleta.find({},function(err,bicis){
        res.status(200).json({
            bicicletas: bicis
        })
    })
}
exports.bicicleta_create = function(req,res){
    var bici = new bicicleta({code: req.body.code, color: req.body.color, modelo : req.body.modelo, ubicacion : [req.body.lat,req.body.lng] })
    
    bici.save(function(err){
        res.status(200).json(bici)
    })
  
}


exports.bicicleta_delete = function (req, res) {
    bicicleta.deleteOne({code: req.body.code},function(err){
        res.status(204).send();
    });  
};

/*
exports.bicicleta_list= function(req,res){
    res.status(200).json({
        bicicleta: Bicicleta.allbicis
    })
}

exports.bicicleta_create = function(req,res){
    var bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo)
    bici.ubicacion = [req.body.lat,req.body.lng];
    Bicicleta.add(bici)
    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete=function(req,res){
    Bicicleta.removeById(req.body.id)
    res.status(204).send()
}*/