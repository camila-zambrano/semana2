var express = require('express')
var router= express.Router();
var usuariocontroller= require('../../controllers/api/usuarioControllerAPI')

router.get('/',usuariocontroller.usuarios_list)
router.post('/create',usuariocontroller.usuarios_create)
router.post('/reservar',usuariocontroller.usuarios_reservar)

module.exports= router