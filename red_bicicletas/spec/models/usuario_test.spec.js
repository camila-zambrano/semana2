var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');
var usuario = require('../../models/usuario');
var reserva = require('../../models/reserva');


describe('testing bicicleta', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console,"mongodb conection error:"))
        db.once('open', function(){
            console.log('we are conected to test database')
            done()
        });
    });

afterEach(function(done){
         reserva.deleteMany({}, function(err,success){
             if(err) console.log(err)
             usuario.deleteMany({},function(err,success){
                if(err) console.log(err)
                Bicicleta.deleteMany({}, function(err,success){
                    if(err) console.log(err)
                    mongoose.connection.close()
                    done()
                })

             })



         })
    })

 
    describe("get bicicletas /",()=>{
        it("status 200", (done) => {

                const bicicleta1 = new Bicicleta({code: 1, color:"verde", modelo:"urbana", lat: -34, lng:-54});
                bicicleta1.save(function (err) {
                    if (err) return handleError(err);
                    const usuario1 = new usuario({nombre: 'Miguel'});
                    usuario1.save(function (err) {
                        if (err) return handleError(err);
                        // saved!
                        var hoy= new Date()
                        var mañana = new Date()
                        mañana.setDate(hoy.getDate() + 1)
                        
                        usuario1.reservar(bicicleta1.id, hoy, mañana, function(err,reserva1){
                            
                            reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err,reservas){
                                
                                expect(reservas.length).toBe(1);
                                expect(reservas[0].diasDereserva()).toBe(2);
                                expect(reservas[0].bicicleta.code).toBe(1);
                                expect(reservas[0].usuario.nombre).toBe(usuario1.nombre);
                                done()
                            })
                
                        })

                        
                      });                                     
                  });                         
        })
    })
})

