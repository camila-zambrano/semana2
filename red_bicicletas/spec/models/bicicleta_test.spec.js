var mongoose = require('mongoose')
var bicicleta = require('../../models/bicicleta');

describe('testing bicicleta', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console,"mongodb conection error:"))
        db.once('open', function(){
            console.log('we are conected to test database')
            done()
        });
    });

    afterEach(function(done){
        bicicleta.deleteMany({}, function(err,success){
            if(err) console.log(err)
            mongoose.connection.close()
            done()
        })
    })

    describe('bicicleta.createInstance',()=>{
        it('crea una instancia de Bicicleta',()=>{
            var bici = bicicleta.createInstance(1,"verde","urbana",[-34.5,-54.1]);

            expect(bici.code).toBe(1)
            expect(bici.color).toBe("verde")

        })
    })

    describe('Bicicleta.allbicis',()=>{
        it('comienza vacia',(done) => {
            bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0)
                
                done()
            })
        })

    })

    describe('Bicicleta.add',()=>{
        it('agrega solo una bici',(done) =>{
            var abici = new bicicleta({code:1,color:"verde",modelo:"urbana"});
            bicicleta.add(abici, function(err, newbici){
                if(err)console.log(err)
                bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(abici.code)
                    done();
                })
               
            })
        })
    })

    describe('bicicleta.findbycode',() => {
        it('debe devolver la bici con code 1',(done) =>{
            bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toEqual(0);
                
                var abici = new bicicleta({code:1,color:"verde",modelo:"urbana"});
                bicicleta.add(abici, function(err, newbici){
                    if(err)console.log(err)

                    var abici2 = new bicicleta({code:2,color:"roja",modelo:"urbana"});

                    bicicleta.add(abici2, function(err, newbici){
                        if(err)console.log(err)

                        bicicleta.findBycode(2, function(error, targetBici){
                            expect(targetBici.code).toBe(abici2.code)
                            expect(targetBici.color).toBe(abici2.color)

                            done()
                        })
                    })
                })


            })
        })


    })



})




/*


beforeEach(() => {bicicleta.allbicis = []})
describe('Bicicleta.allbicis',() => {
    it('comienza vacia', () => {
        expect(bicicleta.allbicis.length).toBe(0);
    })
})

describe('Bicicleta.add',() => {
    it('Agregamos una', () => {
        expect(bicicleta.allbicis.length).toBe(0);

        var a= new bicicleta(1,'rojo','urbana',[6.1906914, -75.5943078])
        bicicleta.add(a)
        expect(bicicleta.allbicis.length).toBe(1);
        expect(bicicleta.allbicis[0]).toBe(a);

    })
})

describe('Bicicleta.findById',() => {
    it('debe devolver la bici con id 1', () => {
        expect(bicicleta.allbicis.length).toBe(0);

        var a= new bicicleta(1,'rojo','urbana',[6.1906914, -75.5943078])
        var b= new bicicleta(2,'blanca','urbana',[-34.596932,-58.3808287])
        bicicleta.add(a)
        bicicleta.add(b)
        var targetBici = bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(a.color);

    })
})

*/


