var bicicleta = require('../../models/bicicleta');
var request = require('request');
var mongoose = require('mongoose')

var base_url = 'http://localhost:5000/api/bicicletas'

describe('testing bicicleta', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console,"mongodb conection error:"))
        db.once('open', function(){
            console.log('we are conected to test database')
            done()
        });
    });

    afterEach(function(done){
        bicicleta.deleteMany({}, function(err,success){
            if(err) console.log(err)
            mongoose.connection.close()
            done()
        })
    })
 
    describe("get bicicletas /",()=>{
        it("status 200", (done) => {
            request.get(base_url, function(error,response,body){
                var result = JSON.parse(body)
                expect(response.statusCode).toBe(200)
                expect(result.bicicletas.length).toBe(0);
                done()
            })
        })
    })

    describe("POST bicicletas /",()=>{
        it("status 200", (done) => {
            var headers={'content-type':'application/json'}
            var aBici = '{"code": 5,"color": "rojo","modelo": "urbano","lat": 6.198881,"lng":-75.580223}' 
            request.post({
                headers:headers,
                url:'http://localhost:5000/api/bicicletas/create', 
                body: aBici
            }, function(error,response,body){
                
                expect(response.statusCode).toBe(200)
                var bici = JSON.parse(body);
                console.log(bici)
                expect(bici.color).toBe("rojo");
                expect(bici.code).toBe(5);
                done()
            })
        })
    })

    describe('POST BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng":-54}';
            request.post({  
                headers: headers,
                url: base_url + '/create',
                body:  aBici
                },function(error, response, body){
                    request.get(base_url, function(error, response, body){
                        var result = JSON.parse(body);
                        expect(result.bicicletas.length).toBe(1);
                        request.delete({
                            headers: headers,
                            url: base_url + '/delete',
                            body: '{ "code": 10}',
                            },function(error, response, body){
                                request.get(base_url, function(error, response, body){
                                    var result = JSON.parse(body);
                                    console.log(result);
                                    expect(result.bicicletas.length).toBe(0);
                                    done();
                                });
                        });
                    });
                });
        });
    });


})

/*
describe('Bicicleta Api', () => {

    describe('GET BICICLETA /', () =>{
        it('sattus 200', (done) => {
            expect(bicicleta.allbicis.length).toBe(2);
            var a= new bicicleta(3,'verde','urbana2',[6.1906914, -75.5943078])
            bicicleta.add(a)
    
            request.get('http://localhost:5000/api/bicicletas', {json:true},(error, response, body)=>{
               console.log(bicicleta.allbicis)
               console.log(body)
               expect(response.statusCode).toBe(200);
                done()
            })
        })
    })
    describe('POST BICICLETA /CREATE', () =>{
        it('status 200', (done) => {
            var headers={'content-type':'application/json'}
            var aBici = '{"id": 5,"color": "rojo","modelo": "urbano","lat": 6.198881,"lng":-75.580223}'    
            request.post({
                headers:headers,
                url:'http://localhost:5000/api/bicicletas/create', 
                body: aBici
            },(error, response, body)=>{
               console.log(body)
               console.log(bicicleta.allbicis)
               
               expect(response.statusCode).toBe(200);
                done()
            })
        })
    })
    
})*/