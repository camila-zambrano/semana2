
var mongoose = require('mongoose');
var schema = mongoose.Schema;

var bicicletaschema = new schema({
    code : Number,
    color : String,
    modelo : String,
    ubicacion : {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }

})

bicicletaschema.methods.toString= function(){
    return 'code' + this.code + "|color:" + this.color;
}

bicicletaschema.statics.allBicis = function(cb){
    return this.find({} , cb)
}

bicicletaschema.statics.add = function(abici,cb){
    return this.create(abici , cb)
}


bicicletaschema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    })
}


bicicletaschema.statics.findBycode = function(acode,cb){
    return this.findOne({code: acode} , cb)
}

bicicletaschema.statics.removeBycode = function(acode,cb){
    return this.deleteOne({code: acode} , cb)
}


module.exports = mongoose.model('Bicicleta', bicicletaschema)
/*
var bicicleta = function (id,color,modelo,ubicacion){
    this.id=id
    this.color=color
    this.modelo=modelo
    this.ubicacion=ubicacion
}

bicicleta.prototype.toString= function(){
    return 'id' + this.id + "|color:" + this.color;
}
bicicleta.allbicis=[];

bicicleta.add=function(abici){
    bicicleta.allbicis.push(abici)
}

bicicleta.findById = function(aBiciId){
    var aBici= bicicleta.allbicis.find(x => x.id == aBiciId)
    if(aBici)
        return aBici
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId} ${bicicleta.allbicis} `)

}
bicicleta.removeById = function(aBiciId){
    for(var i=0; i<bicicleta.allbicis.length; i++){
        if(bicicleta.allbicis[i].id == aBiciId){
            bicicleta.allbicis.splice(i,1);
            break;
        }
    }
}

var a= new bicicleta(1,'rojo','urbana',[6.1906914, -75.5943078])
var b= new bicicleta(2,'blanca','urbana',[-34.596932,-58.3808287])
bicicleta.add(a)
bicicleta.add(b)

module.exports = bicicleta;
*/