var mongoose = require('mongoose');
var schema = mongoose.Schema;
var moment = require('moment')

var reservaSchema = new schema({
    desde: Date,
    hasta:Date,
    bicicleta: {type: mongoose.Schema.Types.ObjectId, ref:'Bicicleta'},
    usuario: {type: mongoose.Schema.Types.ObjectId, ref:'usuario'}
})

reservaSchema.methods.diasDereserva = function(){
    return moment(this.hasta).diff(moment(this.desde),'days') + 1;
}

module.exports = mongoose.model('reserva', reservaSchema)